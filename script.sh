#!/bin/bash

echo "=== BUILDING IMAGE - START ==="
docker-compose up -d
echo "=== BUILDING IMAGE - END ==="

echo "=== PERMISSION - START ==="
chmod -R 777 writable/
ls -ltr
echo "=== PERMISSION - END ==="