<?php

namespace App\Models;

use CodeIgniter\Test\CIUnitTestCase;

class ModelTest extends CIUnitTestCase
{
    public function testProperty()
    {
        $obj = new AtletaModel();
        $this->assertEquals('atleta', $this->getPrivateProperty($obj, 'table'));
        $this->assertEquals('id', $this->getPrivateProperty($obj, 'primaryKey'));
        $allowedFields = ['cpf','data_nascimento', 'nome'];
        $this->assertEquals($allowedFields, $this->getPrivateProperty($obj, 'allowedFields'));

        $obj = new AtletaProvaModel();
        $this->assertEquals('atleta_prova', $this->getPrivateProperty($obj, 'table'));
        $allowedFields = ['atleta_id','prova_id'];
        $this->assertEquals($allowedFields, $this->getPrivateProperty($obj, 'allowedFields'));

        $obj = new ProvaModel();
        $this->assertEquals('prova', $this->getPrivateProperty($obj, 'table'));
        $this->assertEquals('id', $this->getPrivateProperty($obj, 'primaryKey'));
        $allowedFields = ['tipo_prova','data'];
        $this->assertEquals($allowedFields, $this->getPrivateProperty($obj, 'allowedFields'));

        $obj = new ResultadoModel();
        $this->assertEquals('resultado', $this->getPrivateProperty($obj, 'table'));
        $this->assertEquals('id', $this->getPrivateProperty($obj, 'primaryKey'));
        $allowedFields = ['atleta_id','prova_id','horario_inicio','horario_final'];
        $this->assertEquals($allowedFields, $this->getPrivateProperty($obj, 'allowedFields'));
    }
}