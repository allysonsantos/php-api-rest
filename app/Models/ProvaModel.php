<?php namespace App\Models;

use CodeIgniter\Model;

class ProvaModel extends Model
{
    protected $table = 'prova';
    protected $primaryKey = 'id';
    protected $allowedFields = ['tipo_prova','data'];

    public function get($id = null)
    {
        $db = \Config\Database::connect();
        $builder = $db->table($this->table);
        return $builder->getWhere(['id' => $id])->getResult();
    }
}