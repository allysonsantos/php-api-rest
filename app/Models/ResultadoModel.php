<?php namespace App\Models;

use CodeIgniter\Model;

class ResultadoModel extends Model
{
    protected $table = 'resultado';
    protected $primaryKey = 'id';
    protected $allowedFields = ['atleta_id','prova_id','horario_inicio','horario_final'];

    public function generalListing()
    {
        $db = \Config\Database::connect();
        $query = '
                    SELECT
                        P.id AS "id_da_prova",
                        P.tipo_prova AS "tipo_de_prova",
                        A.id AS "id_do_corredor",
                        YEAR(
                            FROM_DAYS(
                                TO_DAYS(NOW()) - TO_DAYS(A.data_nascimento))
                            ) AS idade,
                            A.nome AS "nome_do_corredor",
                            TIMEDIFF(
                                R.horario_final,
                                R.horario_inicio
                            ) AS tempo
                        FROM
                            resultado AS R
                        INNER JOIN atleta AS A
                        ON
                            R.atleta_id = A.id
                        INNER JOIN prova AS P
                        ON
                            R.prova_id = P.id
                        ORDER BY tempo';
        return $db->query($query)->getResult();
    }

}