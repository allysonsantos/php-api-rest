<?php namespace App\Models;

use CodeIgniter\Model;

class AtletaProvaModel extends Model
{
    protected $table = 'atleta_prova';
    protected $allowedFields = ['atleta_id','prova_id'];

    public function get($filter = "")
    {
        $db = \Config\Database::connect();
        $query = "
                SELECT
                    *
                FROM
                    atleta
                INNER JOIN atleta_prova ON atleta.id = atleta_prova.atleta_id
                INNER JOIN prova ON atleta_prova.prova_id = prova.id
                WHERE " . $filter;
        return $db->query($query)->getResult();
    }
}