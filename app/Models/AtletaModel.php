<?php namespace App\Models;

use CodeIgniter\Model;

class AtletaModel extends Model
{
    protected $table = 'atleta';
    protected $primaryKey = 'id';
    protected $allowedFields = ['cpf','data_nascimento', 'nome'];

    public function get($id = null)
    {
        $db = \Config\Database::connect();
        $builder = $db->table($this->table);
        return $builder->getWhere(['id' => $id])->getResult();
    }
}