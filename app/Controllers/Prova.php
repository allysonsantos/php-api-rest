<?php namespace App\Controllers;

use CodeIgniter\RESTful\ResourceController;

class Prova extends ResourceController
{

    protected $modelName = 'App\Models\ProvaModel';
    protected $format = 'json';

    public function create()
    {
        if ($this->request)
        {
            $request = (array) $this->request->getJSON();
            $allowedFields = $this->model->allowedFields;

            if(count($request) == count($allowedFields)) {

                $json = $this->request->getJSON();

                $insert = $this->model->insert([
                    'tipo_prova'    => $json->tipo_prova,
                    'data'          => $json->data
                ]);

                if(!$insert) 
                    return $this->respond(["message" => "Não foi possível registrar a prova!"], 200);

                return $this->respond(["message" => "Prova registrada com sucesso!"], 201);
            } else {
                return $this->respond(["message" => "Todos os campos são obrigatórios!", "fields" => $allowedFields], 200);
            }
        }
    }
}