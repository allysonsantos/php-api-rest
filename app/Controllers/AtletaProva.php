<?php namespace App\Controllers;

use CodeIgniter\RESTful\ResourceController;
use App\Models\AtletaModel;
use App\Models\ProvaModel;
class AtletaProva extends ResourceController
{

    protected $modelName = 'App\Models\AtletaProvaModel';
    protected $format = 'json';

    private function validateData($data) {
        $message = [];

        $atletaModel = new AtletaModel();
        $atleta = $atletaModel->get($data->atleta_id);
        if(!$atleta) $message[] = "Atleta não encontrado!";

        $provaModel = new ProvaModel();
        $prova = $provaModel->get($data->prova_id);
        if(!$prova) $message[] = "Prova não encontrado!";
        else {
            $date = $prova[0]->data;
            $filter = "atleta_prova.atleta_id = " . $data->atleta_id . " AND prova.data LIKE '" . $date . "'";
            $atletaProva = $this->model->get($filter);
            if($atletaProva)
                $message[] = "Não é permitido cadastrar o mesmo corredor em duas provas diferentes na mesma data!";
        }

        return ($message) ? ["status" => false, "message" => $message] : ["status" => true, "message" => ""];
    }

    public function create()
    {
        if ($this->request)
        {
            $request = (array) $this->request->getJSON();
            $allowedFields = $this->model->allowedFields;

            if(count($request) == count($allowedFields)) {

                $json = $this->request->getJSON();
                $result = (object) $this->validateData($json);

                if($result->status) {
                    $insert = $this->model->insert([
                        'atleta_id'    => $json->atleta_id,
                        'prova_id'     => $json->prova_id
                    ]);

                    return $this->respond(["message" => "Corredor registrado na prova com sucesso!"], 201);
                } else {
                    return $this->respond(["message" => $result->message], 200);
                }
            } else {
                return $this->respond(["message" => "Todos os campos são obrigatórios!", "fields" => $allowedFields], 200);
            }
        }
    }
}