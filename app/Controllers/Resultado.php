<?php namespace App\Controllers;

use CodeIgniter\RESTful\ResourceController;

class Resultado extends ResourceController
{

    protected $modelName = 'App\Models\ResultadoModel';
    protected $format = 'json';

    private function treatData($list, $ageRange = false) {
        $mount = [];
        $result = [];
        if(count($list) > 0) {
            // TEST TYPE
            foreach ($list as $value) {
                $mount[$value->tipo_de_prova][] = $value;
            }
            // POSITION
            foreach ($mount as $data) {
                foreach ($data as $key => $value) {
                    $position = $key + 1;
                    $value->posicao = $position . "º";
                }
            }
            // AGE RANGE
            if($ageRange) {
                foreach ($mount as $k1 => $data) {
                    foreach ($data as $value) {
                        $idade = $value->idade;
                        if($idade >= 18 && $idade <= 25)
                            $result[$k1]["18-25"][] = $value;
                        if($idade > 25 && $idade <= 35)
                            $result[$k1]["26-35"][] = $value;
                        if($idade > 35 && $idade <= 45)
                            $result[$k1]["36-45"][] = $value;
                        if($idade > 45 && $idade <= 55)
                            $result[$k1]["46-55"][] = $value;
                        if($idade > 55)
                            $result[$k1][">55"][] = $value;
                    }
                }
            } else {
                $result = $mount;
            }
        }
        return $result;
    }

    public function general()
    {
        $data = $this->model->generalListing();
        $result = $this->treatData($data);
        return $this->respond($result, 200);
    }

    public function age()
    {
        $data = $this->model->generalListing();
        $result = $this->treatData($data, true);
        return $this->respond($result, 200);
    }

    public function create()
    {
        if ($this->request)
        {
            $request = (array) $this->request->getJSON();
            $allowedFields = $this->model->allowedFields;

            if(count($request) == count($allowedFields)) {

                $json = $this->request->getJSON();

                $insert = $this->model->insert([
                    'atleta_id'         => $json->atleta_id,
                    'prova_id'          => $json->prova_id,
                    'horario_inicio'    => $json->horario_inicio,
                    'horario_final'     => $json->horario_final,
                ]);

                if(!$insert) 
                    return $this->respond(["message" => "Não foi possível registrar o resultado!"], 200);

                return $this->respond(["message" => "Resultado registrado com sucesso!"], 201);
            } else {
                return $this->respond(["message" => "Todos os campos são obrigatórios!", "fields" => $allowedFields], 200);
            }
        }
    }
}