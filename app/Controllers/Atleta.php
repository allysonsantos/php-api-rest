<?php namespace App\Controllers;

use CodeIgniter\RESTful\ResourceController;
use DateTime;

class Atleta extends ResourceController
{

    protected $modelName = 'App\Models\AtletaModel';
    protected $format = 'json';

    private function calculateAge($birthDate) {
        $date = new DateTime($birthDate);
        $interval = $date->diff( new DateTime( date('Y-m-d') ) );
        return $interval->format('%Y');
    }

    public function index()
    {
        $data = $this->model->findAll();
        return $this->respond($data, 200);
    }

    public function create()
    {
        if ($this->request)
        {
            $request = (array) $this->request->getJSON();
            $allowedFields = $this->model->allowedFields;

            if(count($request) == count($allowedFields)) {

                $json = $this->request->getJSON();

                if($this->calculateAge($json->data_nascimento) < 18)
                    return $this->respond(["message" => "Não é permitida a inscrição de menores de idade!"], 200);

                $insert = $this->model->insert([
                    'nome'              => $json->nome,
                    'cpf'               => $json->cpf,
                    'data_nascimento'   => $json->data_nascimento
                ]);

                if(!$insert)
                    return $this->respond(["message" => "Não foi possível registrar corredor!"], 200);

                return $this->respond(["message" => "Corredor registrado com sucesso!"], 201);
            } else {
                return $this->respond(["message" => "Todos os campos são obrigatórios!", "fields" => $allowedFields], 200);
            }
        }
    }
}