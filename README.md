PHP - API REST
====================

# Tecnologias utilizadas

* PHP 7.4.15
* Apache 2.4.38
* MariaDB 10.5.8
* phpMyAdmin 5.0.4
* CodeIgniter 4.1.1

## Executar a criação do ambiente

### Basta executar o comando abaixo na raiz do projeto:

```
./script.sh
```
## Acesso

### Web:

```
http://localhost:4500/
```
### phpMyAdmin:

```
http://localhost:4700/
```

```
login: root
senha: root
```

## Teste unitário
Para executar o teste unitário precisa acessar o container:


```
docker-compose exec web /bin/bash
```

Após acessar o container **web**, precisa instalar os pacotes:

```
composer install
```

Por fim, basta executar o comando abaixo para realizar os testes unitários:

```
./vendor/bin/phpunit
```

## Exemplos de requisição e retorno


![picture](images/post-atleta.png)

![picture](images/post-prova.png)

![picture](images/post-atleta-prova.png)

![picture](images/post-resultado.png)

![picture](images/get-idade.png)

![picture](images/get-geral.png)