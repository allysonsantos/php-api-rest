-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: db
-- Tempo de geração: 22-Fev-2021 às 21:24
-- Versão do servidor: 10.5.8-MariaDB-1:10.5.8+maria~focal
-- versão do PHP: 7.4.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `atletismo`
--

--
-- Extraindo dados da tabela `atleta`
--

INSERT INTO `atleta` (`id`, `nome`, `cpf`, `data_nascimento`) VALUES
(1, 'Allyson', '1237654523', '1998-01-21'),
(2, 'Daniel', '1237654523', '1997-01-21'),
(3, 'Elena', '1237654523', '1995-01-21'),
(4, 'Jenifer', '1237654523', '1990-01-21'),
(5, 'Lucas', '1237654523', '1985-01-21');

--
-- Extraindo dados da tabela `prova`
--

INSERT INTO `prova` (`id`, `tipo_prova`, `data`) VALUES
(1, '5km', '2021-03-10'),
(2, '10km', '2021-03-12'),
(3, '15km', '2021-03-10'),
(4, '20km', '2021-03-15');

--
-- Extraindo dados da tabela `atleta_prova`
--

INSERT INTO `atleta_prova` (`atleta_id`, `prova_id`) VALUES
(1, 1),
(1, 2),
(2, 1),
(2, 2),
(3, 1),
(4, 1),
(5, 1);

--
-- Extraindo dados da tabela `resultado`
--

INSERT INTO `resultado` (`id`, `atleta_id`, `prova_id`, `horario_inicio`, `horario_final`) VALUES
(1, 1, 1, '10:00:00', '10:10:00'),
(2, 2, 1, '10:00:00', '10:12:00'),
(3, 3, 1, '10:00:00', '10:15:00'),
(4, 4, 1, '10:00:00', '10:17:00'),
(5, 5, 1, '10:00:00', '10:20:00'),
(7, 1, 2, '11:00:00', '11:10:00'),
(8, 2, 2, '11:00:00', '11:11:00');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
