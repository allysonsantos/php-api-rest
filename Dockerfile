FROM php:7.4.15-apache-buster

ENV COMPOSER_HOME="/var/www/html/"
WORKDIR /var/www/html
EXPOSE  3306

# Install Packages
RUN apt-get update -y
RUN apt-get install libicu-dev -y
RUN apt-get install zip -y
RUN apt-get install unzip -y
RUN docker-php-ext-install mysqli pdo pdo_mysql
RUN docker-php-ext-configure intl
RUN docker-php-ext-install intl

# Added module rewrite
RUN a2enmod rewrite

# Install Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin --filename=composer